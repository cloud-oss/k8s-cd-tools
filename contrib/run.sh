#!/bin/sh
set -e

# --------------------------------
# Parameters

KUBERNETES_IMAGE=registry.gitlab.com/cloud-oss/k8s-cd-tools
KUBERNETES_VERSION=1.23


# --------------------------------
# Tests

# shellcheck disable=SC1091
if [ -f "$(pwd)"/.env ]; then
    echo "Using local .env file"
    # shellcheck source=/dev/null
    . "$(pwd)"/.env
fi


# --------------------------------
# Functions

# pull container image
if [ -z "$DISABLE_PULL" ]; then
   docker pull "$KUBERNETES_IMAGE":"$KUBERNETES_VERSION" | grep -e Status -e Error
fi

# --------------------------------
# Run

DOCKER_ARGS="--mount type=bind,source="$(pwd)",target=/source \
             --network host \
             --workdir /source"

if [ -n "$KUBECONFIG" ]; then
  echo "KUBECONFIG: ${KUBECONFIG}"
  DOCKER_ARGS="$DOCKER_ARGS \
               --mount type=bind,source=${KUBECONFIG},target=/kubeconfig,readonly \
               --env KUBECONFIG=/kubeconfig"
fi

if [ -n "$SOPS_AGE_KEY_FILE" ]; then
   echo "SOPS_AGE_KEY_FILE: ${SOPS_AGE_KEY_FILE}"
   DOCKER_ARGS="$DOCKER_ARGS \
                --mount type=bind,source=${SOPS_AGE_KEY_FILE},target=/age_key,readonly \
                --env SOPS_AGE_KEY_FILE=/age_key"
fi

# shellcheck disable=SC2086
docker run -it --rm --name kustomize $DOCKER_ARGS "$KUBERNETES_IMAGE":"$KUBERNETES_VERSION"
