# k8s-cd-tools

Alpine based Image for kubernetes continuous deployment based on common gitops toolchain.

Source:
- [kubectl](https://github.com/kubernetes/kubectl)
- [kustomize](https://github.com/kubernetes-sigs/kustomize)
- [kustomize-sops](https://github.com/viaduct-ai/kustomize-sops)
- [kubeconform](https://github.com/yannh/kubeconform)
- [kubernetes-json-schema](https://github.com/yannh/kubernetes-json-schema)
- [sops](https://github.com/mozilla/sops)
- [age-encryption](https://github.com/FiloSottile/age)


### Features
- version specific json-schemas are included in filesystem
- hook-scripts for validations
- openapi2jsonschema.py included for easy extending use of image
- additional tools: git, yq


### Requirements

This image should be used with typical kustomize file structure, e.g.

```
filesystem/
├── base/
│   └── microservice/
│       ├── kustomization.yml     # base kustomization
│       ├── deployment.yml
│       └── configmap.yml
├── overlays/
│   └── microservice/
│       ├── kustomization.yml     # overlay kustomization
│       ├── ksops.yml             # kustomize-sops generator
│       └── secret.yml            # encrypted secret
└── .sops.yaml
```


### Run

Create a local *.env* file and insert the following parameters

```
KUBECONFIG=/path/to/kubeconfig
SOPS_AGE_KEY_FILE=/path/to/sops/age/key
```

Place the script contrib/run.sh into your git root and start the container
```
./run.sh
```

Use Kustomize to deploy manifests

```
kustomize build overlays/something/ | kubectl apply -f -
```


### Hooks
The hooks directory includes scripts for automating things, e.g. pre-commit or merge requests
These scripts should be run from inside 

> /hooks/kustomize.sh <path>

check if manifests can be generated

> /hooks/kubeconform.sh <path>

check if generated manifests by kustomize are valid

> /hooks/secrets.sh <path>

check if secrets are properly encrypted
