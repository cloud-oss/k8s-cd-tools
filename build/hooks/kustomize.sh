#!/bin/sh
set -eu

SUCCESS="true"
DIR="$1"
RESULT=$(find "$DIR" -type d -exec sh -c 'ls -1 "$1" | grep -i -q -E "^kustomization\.(yml|yaml)$"' _ {} ';' -print)

for ITEM in $RESULT; do
   if OUTPUT=$(kustomize build "$ITEM" 2>/dev/null); then
      echo "✓ $ITEM"
   else
      echo "✗ ERROR FOR $ITEM"
      echo "$OUTPUT"
      SUCCESS="false"
   fi
done

if [ "${SUCCESS}" != "true" ]; then exit 1; fi
