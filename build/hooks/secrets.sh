#!/bin/sh
set -eu

SUCCESS="true"
DIR="$1"
RESULT=$(find "$DIR" -type f -name '*.yml' -or -name '*.yaml' | ( xargs grep -r -l -E "^kind: Secret$|^kind: Config$" || true ) | paste -s -d " ")

for ITEM in $RESULT; do
   if grep -q -x -E "^sops:$" "$ITEM"; then
      echo "✓ $ITEM"
   else
      echo "✗ ERROR --> MISSING ENCRYPTION FOR $ITEM"
      SUCCESS="false"
   fi
done;

if [ "${SUCCESS}" != "true" ]; then exit 1; fi
